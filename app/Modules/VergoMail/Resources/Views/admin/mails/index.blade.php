@extends('vergo_base::admin.layouts.all')

@section('page_title', ' Рассылки<small>отчет отправки писем</small>')

@section('tHead')
        <th>Пользователь</th>
        <th>Сценарий</th>
        <th>Название письма</th>
        <th>Код ответа</th>
        <th>Дата отправки</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->user->getFullName()}}</td>
            <td>{{$model->scenario->name}}</td>
            <td>{{$model->scenario->template->title}}</td>
            <td>{{$model->response_code}}</td>
            <td>{{$model->getDate()}}</td>
            <td class="last">
                <a class="btn btn-info" href="{{route('admin:mails:send',['id'=>$model->id])}}">
                    <i class="fa fa-send"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection