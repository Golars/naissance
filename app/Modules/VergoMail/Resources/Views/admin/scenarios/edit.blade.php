@extends('vergo_base::admin.layouts.edit')

@section('title_name','Сценария')

@section('form_body')
{!! Form::hidden('id', $model->id, ['class'=>'form-control'] ) !!}

<div class="form-group">
    {!! Form::label('Название') !!}
    {!! Form::text('name', $model->name, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Шаблон') !!}
    {!! Form::select('templates_id', $templates, $model->templates_id, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Адресат') !!}
    {!! Form::select('role_id', $roles, $model->role_id, ['class'=>'form-control']) !!}
</div>
@endsection
