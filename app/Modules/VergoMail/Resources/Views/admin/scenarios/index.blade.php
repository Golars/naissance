@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Сценарии отправки сообщений<small>управление сценариями</small>')

@section('tHead')
        <th>Название</th>
        <th>Шаблон</th>
        <th>Получатели</th>
        <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}</td>
            <td>{{isset($model->template->title) ? $model->template->title : '-'}}</td>
            <td>{{isset($model->role->name) ? $model->role->name : 'Все'}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="{{route('admin:scenarios:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
                <a class="btn btn-success" href="{{route('admin:scenarios:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:scenarios:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                <a class="btn btn-info" href="{{route('admin:scenarios:send',['id'=>$model->id])}}">
                    <i class="fa fa-send"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection