@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Шаблоны писем<small>управление шаблонами</small>')

@section('tHead')
        <th>Название</th>
        <th>Текст</th>
        <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->title}}</td>
            <td>{{$model->getShortText()}}</td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="{{route('admin:templates:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
                <a class="btn btn-success" href="{{route('admin:templates:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:templates:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection