<?php
namespace App\Modules\VergoMail\Database\Models;

use App\Modules\VergoBase\Database\Models\Base;

class Scenario extends Base {
    public $timestamps = true;
    protected $table = 'scenarios';
    protected $perPage = 15;
    protected $fillable = array(
        'name',
        'role_id',
        'templates_id',
        'status'
    );
    public function role(){
        return $this->hasOne('App\Modules\VergoBase\Database\Models\Role','id','role_id')->where('status',Base::STATUS_ACTIVE);;
    }
    public function template(){
        return $this->hasOne('App\Modules\VergoMail\Database\Models\Templates','id','templates_id')->where('status',Base::STATUS_ACTIVE);;
    }

    public function getDate(){
        return date('H:i:s j M Y',strtotime($this->created_at));
    }
}
