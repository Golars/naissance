<?php
namespace App\Modules\VergoMail\Database\Models;

use App\Modules\VergoBase\Database\Models\Base;

class Templates extends Base {
    public $timestamps = true;
    protected $table = 'templates';
    protected $perPage = 15;
    protected $fillable = array(
        'title',
        'text',
        'status'
    );

    public function getDate(){
        return date('H:i:s j M Y',strtotime($this->created_at));
    }

    public function getShortText(){
        $text = mb_substr(strip_tags($this->text),0,50,'utf-8');
        if(strlen($text) < strlen(strip_tags($this->text))){
            $text = $text . '...';
        }
        return  $text;
    }
}
