<?php
namespace App\Modules\VergoMail\Database\Models;

use App\Modules\VergoBase\Database\Models\Base;

class Mails extends Base {
    public $TYPE = 'mails';
    public $timestamps = true;
    protected $table = 'mails';
    protected $perPage = 15;
    protected $fillable = array(
        'user_id',
        'response_code',
        'scenario_id',
        'status'
    );
    public function user(){
        return $this->hasOne('App\Modules\VergoBase\Database\Models\User','id','user_id');
    }
    public function scenario(){
        return $this->hasOne('App\Modules\VergoMail\Database\Models\Scenario','id','scenario_id');
    }

    public function getDate(){
        return date('H:i:s j M Y',strtotime($this->created_at));
    }
}
