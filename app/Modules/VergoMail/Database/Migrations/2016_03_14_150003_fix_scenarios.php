<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixScenarios extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('scenarios');
		Schema::create('scenarios', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('role_id')->references('id')->on('roles');
			$table->integer('templates_id');
			$table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}
}
