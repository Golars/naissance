<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\VergoBase\Database\Models\Module;

class InitMails extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Module::create([
			'name'				=> 'VERGO Mail',
			'info' 				=> 'Mail Module for [VERGO] core',
			'version' 			=> 1,
			'install_version' 	=> 1,
			'status' 			=> 1
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}
}
