<?php
namespace App\Modules\VergoMail\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class VergoMailServiceProvider extends ServiceProvider
{
	/**
	 * Register the VergoMail module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\VergoMail\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the VergoMail module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('vergo_mail', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('vergo_mail', base_path('resources/views/vendor/vergo_mail'));
		View::addNamespace('vergo_mail', realpath(__DIR__.'/../Resources/Views'));
	}
}
