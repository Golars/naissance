<?php
namespace App\Modules\VergoMail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;

class TemplatesController extends Controller {
    protected $prefix = 'admin:templates';
    protected $moduleName = 'vergo_mail';
    protected $serviceName = 'App\Modules\VergoMail\Http\Services\Templates';

    public function add(Request $request){
        $this->setRules([
            'title'  =>  'required',
            'text'   =>  'required'
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'     =>  'required',
            'title'  =>  'required',
            'text'   =>  'required'
        ]);
        return parent::edit($request, $id);
    }
}