<?php
namespace App\Modules\VergoMail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;

class MailsController extends Controller {
    protected $prefix = 'admin:mails';
    protected $moduleName = 'vergo_mail';
    protected $serviceName = 'App\Modules\VergoMail\Http\Services\Mails';

    public function add(Request $request){
        $this->setRules([
            'user_id'       =>  'required',
            'article_id'    =>  'required',
            'text'          =>  'required|max:200'
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'            =>  'required',
            'user_id'       =>  'required',
            'article_id'    =>  'required',
            'text'          =>  'required|max:200'
        ]);
        return parent::edit($request, $id);
    }

    public function sendMail(Request $request,$id){
        if (!$id){
            return redirect($this->getRoute('index',['error' => 'error']));
        }
        $message = $this->service->getOne($id);
        if (!$message){
            return redirect($this->getRoute('index',['error' => 'error']));
        }
        $this->service->sendMail(
            $message->user,
            'vergo_mail::admin.scenarios.emails.blank',
            $message->scenario,
            ['data'=>[
                'title' =>  $message->scenario->template->title,
                'text'  =>  $message->scenario->template->text
            ]]
        );
        return redirect($this->getRoute());
    }
}