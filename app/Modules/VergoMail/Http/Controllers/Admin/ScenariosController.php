<?php
namespace App\Modules\VergoMail\Http\Controllers\Admin;

use App;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ScenariosController extends Controller {
    protected $prefix = 'admin:scenarios';
    protected $moduleName = 'vergo_mail';
    protected $serviceName = 'App\Modules\VergoMail\Http\Services\Scenarios';

    protected function prepareData($model = null){
        return [
            'model'     =>  (isset($model->id)) ? $model : $this->service->getModel(),
            'roles'     => App::make('App\Modules\VergoBase\Http\Services\Role')->prepareSelect(),
            'templates' => App::make('App\Modules\VergoMail\Http\Services\Templates')->prepareSelect(),
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'name'  =>  'required',
            'role_id'   =>  'required',
            'templates_id'   =>  'required'
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'     =>  'required',
            'name'  =>  'required',
            'role_id'   =>  'required',
            'templates_id'   =>  'required'
        ]);
        return parent::edit($request, $id);
    }

    public function sendMail(Request $request,$id){
        if(!$id){
            return redirect($this->getRoute('index',['error' => 'error']));
        }
        $this->service->setWhere(['status' => App\Modules\VergoBase\Database\Models\Base::STATUS_ACTIVE]);
        $message = $this->service->getOne($id);
        if(!$message && !isset($message->template)){
            return redirect($this->getRoute('index',['error' => 'error']));
        }

        $users = new App\Modules\VergoBase\Http\Services\User();
        if($message->role_id){
            $users->setWhere(['role_id'=>$message->role_id]);
        }
        $sender = new App\Modules\VergoMail\Http\Services\Mails();

        $sender->sendMail($users->getAll(),
            $this->getViewRoute('emails.blank'),
            $message,
            ['data'=>[
                'title' =>  $message->template->title,
                'text'  =>  $message->template->text
            ]]);
        return redirect($this->getRoute());
    }
}