<?php
namespace App\Modules\VergoMail\Http\Services;

use Illuminate\Support\Facades\Mail;
use App\Modules\VergoBase\Http\Services\Base;

class Mails extends Base
{
    protected $modelName = 'App\Modules\VergoMail\Database\Models\Mails';

    public function sendMail($users, $view = 'vergo_mail::mails.blank', $model, $data = []){
        if(isset($users->id)){
            $users = [$users];
        }
        Mail::send($view, $data, function ($message) use ($users, $model) {
            $message->from(env('MAIL_SENDER', 'admin@vergo.space'), env('MAIL_SENDER_NAME', '[VERGO]'));
            foreach ($users as $user) {
                $message->to($user->email, $user->getFullName())->subject($model->name);
                $this->getModel()->create([
                    'user_id'       =>  $user->id,
                    'response_code' =>  200,
                    'scenario_id'   =>  $model->id
                ]);
            }
        });

    }
}