<?php
namespace App\Modules\VergoMail\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class Scenarios extends Base
{
    protected $modelName = 'App\Modules\VergoMail\Database\Models\Scenario';
}