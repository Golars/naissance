<?php
namespace App\Modules\VergoMail\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class Templates extends Base
{
    protected $modelName = 'App\Modules\VergoMail\Database\Models\Templates';

    public function prepareSelect(){
        $templates = [];
        $this->setWhere(['status'=>\App\Modules\VergoBase\Database\Models\Base::STATUS_ACTIVE]);
        foreach($this->getAll() as $template){
            $templates[$template->id] = $template->title;
        }

        return $templates;
    }
}