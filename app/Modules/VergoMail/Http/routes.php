<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function() use ($asPrefix) {
		Route::group(['prefix'=>'/mails'],function() use ($asPrefix){
			$asAction = ':mails';
			Route::get('/', ['as' => $asPrefix . $asAction .':index',  'uses' => 'Admin\MailsController@index']);
			Route::get('/{id}/send',['as' => $asPrefix .$asAction .':send',  'uses' => 'Admin\MailsController@sendMail']);
		});
		Route::group(['prefix' =>'templates'],function() use ($asPrefix){
			$adAction = ':templates';
			Route::get('/',['as'=> $asPrefix . $adAction. ':index', 'uses' =>'Admin\TemplatesController@index']);
			Route::any('/add',['as'=> $asPrefix . $adAction.':add', 'uses' =>'Admin\TemplatesController@add']);
			Route::any('/{id}/del',['as'=> $asPrefix . $adAction.':delete', 'uses' =>'Admin\TemplatesController@delete']);
			Route::any('/{id}/edit',['as'=> $asPrefix .$adAction. ':edit', 'uses' =>'Admin\TemplatesController@edit']);
			Route::get('/{id}/active',['as'=> $asPrefix .$adAction. ':active', 'uses' =>'Admin\TemplatesController@active']);
		});
		Route::group(['prefix' =>'scenarios'],function() use ($asPrefix){
			$adAction = ':scenarios';
			Route::get('/',['as'=> $asPrefix . $adAction. ':index', 'uses' =>'Admin\ScenariosController@index']);
			Route::get('/{id}/send',['as'=> $asPrefix . $adAction. ':send', 'uses' =>'Admin\ScenariosController@sendMail']);
			Route::any('/add',['as'=> $asPrefix . $adAction.':add', 'uses' =>'Admin\ScenariosController@add']);
			Route::any('/{id}/del',['as'=> $asPrefix . $adAction.':delete', 'uses' =>'Admin\ScenariosController@delete']);
			Route::any('/{id}/edit',['as'=> $asPrefix .$adAction. ':edit', 'uses' =>'Admin\ScenariosController@edit']);
			Route::get('/{id}/active',['as'=> $asPrefix .$adAction. ':active', 'uses' =>'Admin\ScenariosController@active']);
		});
	});
});
