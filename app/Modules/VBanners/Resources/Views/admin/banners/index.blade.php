@extends('vergo_base::admin.layouts.all')

@section('page_title', ' Баннеры<small>управление рекламой</small>')

@section('tHead')
        <th>Id</th>
        <th>Название</th>
        <th>Позиция</th>
        <th>URL</th>
        <th>Обложка</th>
        <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->id}}</td>
            <td>{{$model->name}}</td>
            <td>{{$model->getPositionName()}}</td>
            <td>{{$model->href}}</td>
            <td><img src="{{$model->cover}}" width="75" class="img-circle"></td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="{{route('admin:banners:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
                <a class="btn btn-success" href="{{route('admin:banners:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:banners:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection