@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Баннера')

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control'] ) !!}

    <div class="form-group">
        {!! Form::label('Название') !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Гиперссылка') !!}
        {!! Form::text('href', $model->href, ['class'=>'form-control'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Обложка') !!}
        {!! Form::text('cover', $model->cover, ['class'=>'form-control'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Позиция') !!}
        {!! Form::select('position', \App\Modules\VBanners\Database\Models\Banners::$positionName,$model->position, ['class'=>'form-control','id'=>'edit'] ) !!}
    </div>
@endsection
