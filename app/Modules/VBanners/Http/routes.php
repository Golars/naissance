<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'vbn'], function() {
	Route::get('/', function () {
		return view('vergo_base::info', ['module' => 'Banners Module']);
	});
});
Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function() use ($asPrefix) {
		Route::group(['prefix'=>'/banners'],function() use ($asPrefix){
			$asAction = ':banners';
			Route::get('/', ['as' => $asPrefix . $asAction .':index',  'uses' => 'Admin\BannersController@index']);
			Route::any('/add', ['as' => $asPrefix .$asAction .':add',  'uses' => 'Admin\BannersController@add']);
			Route::any('/{id}/edit', ['as' => $asPrefix .$asAction .':edit',  'uses' => 'Admin\BannersController@edit']);
			Route::get('/{id}/active',['as' => $asPrefix .$asAction .':active',  'uses' => 'Admin\BannersController@active']);
			Route::get('/{id}/delete',['as' => $asPrefix .$asAction .':delete',  'uses' => 'Admin\BannersController@delete']);
		});
	});
});