<?php

namespace App\Modules\VBanners\Http\Controllers\Admin;

use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class BannersController extends Controller{

    protected $prefix = 'admin:banners';
    protected $moduleName = 'v_banners';
    protected $serviceName = 'App\Modules\VBanners\Http\Services\Banners';

    public function add(Request $request){
        $this->setRules([
            'name'      =>  'required|min:2',
            'href'      =>  'required|min:3',
            'position'  =>  'required',
            'cover'     =>  ''
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'    =>  'required',
            'name'      =>  'required|min:2',
            'href'      =>  'required|min:3',
            'position'  =>  'required',
            'cover'     =>  ''
        ]);
        return parent::edit($request, $id);
    }
}
