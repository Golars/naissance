<?php

namespace App\Modules\VBanners\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class Banners extends Base{
    protected $modelName = 'App\Modules\VBanners\Database\Models\Banners';
}