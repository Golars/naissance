<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\VergoLanguage\Database\Models\Language as Model;

class FixBaseLang extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('language');
		Schema::dropIfExists('languages');
		Schema::create('languages', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('short_name', 2);
			$table->tinyInteger('status')->default(1);
		});
		Model::create([
			'name'			=>	'Русский',
			'short_name'	=>	'ru'
		]);
		Model::create([
			'name'			=>	'English',
			'short_name'	=>	'en'
		]);
		Model::create([
			'name'			=>	'Український',
			'short_name'	=>	'ua',
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('languages');
	}
}
