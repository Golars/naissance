<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\VergoLanguage\Database\Models\Language as Model;

class CreateLanguagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('lang',5);
			$table->string('url');
			$table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
		Model::create([
			'id'	=>	1,
			'name'	=>	'Русский',
			'lang'	=>	'RU',
			'url'	=>	'/'
		]);
		Model::create([
			'id'	=>	2,
			'name'	=>	'English',
			'lang'	=>	'En',
			'url'	=>	'/En'
		]);
		Model::create([
			'id'	=>	3,
			'name'	=>	'Український',
			'lang'	=>	'UA',
			'url'	=>	'/ua'
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('languages');
	}
}
