<?php

namespace App\Modules\VergoLanguage\Database\Models;

use App\Modules\VergoBase\Database\Models\Base;

class Language extends Base {
    public $timestamps = false;

    protected $table = 'languages';
    protected $fillable = array(
        'name',
        'short_name',
        'status'
    );
}