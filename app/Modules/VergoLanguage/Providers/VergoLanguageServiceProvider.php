<?php
namespace App\Modules\VergoLanguage\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class VergoLanguageServiceProvider extends ServiceProvider
{
	/**
	 * Register the VergoLanguage module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\VergoLanguage\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the VergoLanguage module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('vergo_language', realpath(__DIR__.'/../Resources/Lang'));
		View::addNamespace('vergo_language', base_path('resources/views/vendor/vergo_language'));
		View::addNamespace('vergo_language', realpath(__DIR__.'/../Resources/Views'));
	}
}
