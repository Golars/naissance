<?php
return [
    'page_title'        =>  'Языки',
    'page_title_small'  =>  'управление переводами',
    'table_name'        =>  'Название',
    'table_short_name'  =>  'Краткое название',
    'table_address'     =>  'Переадресация'
];