@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Языка')

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label('Название') !!}
        {!! Form::text('name',$model->name, ['class'=>'form-control','required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Url Code') !!}
        {!! Form::text('short_name',$model->short_name, ['class'=>'form-control','required']) !!}
    </div>
@endsection
