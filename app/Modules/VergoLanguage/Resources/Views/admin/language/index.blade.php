@extends('vergo_base::admin.layouts.all')

@section('page_title')
    {{trans('vergo_language::admin.page_title')}}<small>{{trans('vergo_language::admin.page_title_small')}}</small>
@endsection

@section('tHead')
    <th>{{trans('vergo_language::admin.table_name')}}</th>
    <th>{{trans('vergo_language::admin.table_address')}}</th>
@endsection
@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}</td>
            <td>
               {{$model->short_name}}
            </td>
            <td class="last">
                <a class="btn btn-success" href="{{route('admin:language:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:language:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection