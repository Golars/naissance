<?php
namespace App\Modules\VergoLanguage\Http\Controllers\Admin;

use App;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class LanguageController extends Controller {
    protected $prefix = 'admin:language';
    protected $moduleName = 'vergo_language';
    protected $serviceName = 'App\Modules\VergoLanguage\Http\Services\Language';

    protected function prepareData($model = null){
        return [
            'model'     =>  (isset($model->id)) ? $model : $this->service->getModel()
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'name'    =>  'required',
            'lang'    =>  'required|max:5',
            'url'     =>  ''
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'            =>  'required',
            'name'    =>  'required',
            'lang'    =>  'required|max:5',
            'url'     =>  ''
        ]);
        return parent::edit($request, $id);
    }
}