<?php
namespace App\Modules\VergoLanguage\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class Language extends Base {
    protected $orderBy = [
        'id' => 'desc'
    ];
    protected $modelName = 'App\Modules\VergoLanguage\Database\Models\Language';

    public function getAllShortName(){
        $collection = $this->getAll();
        return $collection->map(function($model){
           return $model->short_name;
        })->toArray();
    }
}