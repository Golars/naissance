<?php
namespace App\Modules\VergoLanguage\Http\Middleware;

use App;
use Closure;

class LangMiddleware{
    public function handle($request, Closure $next)
    {
        if($request->getHttpHost() == config('app.url')) {
            return $next($request);
        }

        /*  @var $service App\Modules\VergoLanguage\Http\Services\Language */
        $service = App::make('App\Modules\VergoLanguage\Http\Services\Language');

        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        $service->setWhere(['short_name' => $url_array[0]]);

        if (isset($service->getOne()->id)) {
            App::setLocale($service->getModel()->short_name);
        }

        return $next($request);
    }
}