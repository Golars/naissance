<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin', 'lang']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
		Route::group(['prefix' => 'lang'], function () use ($asPrefix) {
			$asAction = ':language';
			Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\LanguageController@index']);
			Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\LanguageController@add']);
			Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\LanguageController@active']);
			Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\LanguageController@delete']);
			Route::any('/{id}/edit', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\LanguageController@edit']);
		});
	});
});