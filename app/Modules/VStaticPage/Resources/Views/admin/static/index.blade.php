@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Статические страницы<small>управление страницами</small>')

@section('tHead')
        <th>Id</th>
        <th>Название</th>
        <th>Url</th>
        <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->id}}</td>
            <td>{{$model->name}}</td>
            <td>
                <a href="{{route('page',['id'=>$model->url])}}" class="btn btn-info">
                    <i class="fa fa-chain"> {{$model->url}}</i>
                </a>
            </td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="{{route('admin:static:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
                <a class="btn btn-success" href="{{route('admin:static:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:static:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection