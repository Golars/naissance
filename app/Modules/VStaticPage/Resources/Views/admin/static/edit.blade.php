@extends('vergo_base::admin.layouts.edit')

@section('title_name','Статической страницы')

@section('form_body')
{!! Form::hidden('id', $model->id, ['class'=>'form-control'] ) !!}

<div class="form-group">
    {!! Form::label('Название') !!}
    {!! Form::text('name', $model->name, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Гиперссылка') !!}
    {!! Form::text('url', $model->url, ['class'=>'form-control'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Текст') !!}
    {!! Form::textarea('text', $model->text, ['class'=>'form-control','id'=>'edit'] ) !!}
</div>
@endsection

@section('scripts')
    <link href="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/css/themes/gray.min.css')}}" rel="stylesheet" type="text/css">

    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/froala_editor.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/froala_editor_ie8.min.js')}}"></script>
    <![endif]-->
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/tables.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/lists.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/colors.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/media_manager.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/font_family.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/font_size.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/block_styles.min.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('/js/lib/froala_editor/plugins/video.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').editable({
                inlineMode: false,
                theme: 'gray',
                height: '300',
                language: 'ru'
            })
            $('.froala-box div').last().remove()
        });
    </script>
    <!-- Datatables -->
    <script src="{{$app['vergo_base.assets']->getPath('js/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{$app['vergo_base.assets']->getPath('js/lib/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#collection').dataTable({
                "oLanguage": {
                    "sSearch": "Поиск по всех колонках:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "{{$app['vergo_base.assets']->getPath('js/lib/datatables/tools/swf/copy_csv_xls_pdf.swf')}}"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@endsection