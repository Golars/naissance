@extends('v_static_page::site.static.layouts.default')

{{-- Content --}}
@section('content')
    <div id="content" class="row" style="padding-bottom: 40px;">
        <div class="col-lg-12 col-md-12 col-sm-12 content">
            <div class="h2 text-center" style="text-align: center;">
                {{$model->name}}
            </div>
            <?php echo $model->text ?>
        </div>
    </div>
@stop