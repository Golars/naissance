<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
    ================================================== -->
	<meta charset="utf-8" />
	<title>
		@section('title')
			{{$model['name']}}
		@show
	</title>
	@section('meta_keywords')
		<meta name="keywords" content="" />
	@show
	@section('meta_author')
		<meta name="author" content="" />
	@show
	@section('meta_description')
		<meta name="description" content="" />
	@show
</head>
<body>
<!-- Content -->
@yield('content')
<!-- ./ content -->
</body>
</html>