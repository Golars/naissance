<?php

namespace App\Modules\VStaticPage\Http\Controllers;

use App\Modules\VergoBase\Http\Controllers\Controller;

class PagesController extends Controller{

    protected $serviceName = 'App\Modules\VStaticPage\Http\Services\PageService';
    protected $modelName = 'App\Modules\VStaticPage\Database\Models\PageModel';
    protected $moduleName = 'v_static_page';
    protected $prefix = 'site:static';

    public function getPage($url){
        $this->service->setWhere([
            'status'    => \App\Modules\VergoBase\Database\Models\Base::STATUS_ACTIVE,
            'url'      =>  $url
        ]);
        $model = $this->service->getOne();
        if(!$model) {
            return abort(404);
        }
        return view($this->getViewRoute('page'), ['model'=>$model]);
    }
}
