<?php
namespace App\Modules\VStaticPage\Http\Controllers\Admin;

use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    protected $prefix = 'admin:static';
    protected $moduleName = 'v_static_page';
    protected $serviceName = 'App\Modules\VStaticPage\Http\Services\PageService';

    public function add(Request $request){
        $this->setRules([
            'name'  =>  'required|unique:pages|min:3',
            'url'   =>  'required|unique:pages|min:3',
            'text'  =>  'required'
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'    =>  'required',
            'name'  =>  'required|min:2',
            'url'   =>  'required|min:3',
            'text'  =>  'required'
        ]);
        return parent::edit($request, $id);
    }
}
