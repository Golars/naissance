<?php

namespace App\Modules\VStaticPage\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class PageService extends Base{
    protected $modelName = 'App\Modules\VStaticPage\Database\Models\PageModel';
}