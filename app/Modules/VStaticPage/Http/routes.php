<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => 'vsp'], function() {
	Route::get('/', function () {
		return view('vergo_base::info', ['module' => 'Static Pages Module']);
	});
});

Route::get('/page/{url}',['as'=> 'page', 'uses' =>'PagesController@getPage']);

Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function() use ($asPrefix) {
		Route::group(['prefix' =>'static'],function() use ($asPrefix){
			$adAction = ':static';
			Route::get('/',['as'=> $asPrefix . $adAction. ':index', 'uses' =>'Admin\PagesController@index']);
			Route::any('/add',['as'=> $asPrefix . $adAction.':add', 'uses' =>'Admin\PagesController@add']);
			Route::any('/{id}/del',['as'=> $asPrefix . $adAction.':delete', 'uses' =>'Admin\PagesController@delete']);
			Route::any('/{id}/edit',['as'=> $asPrefix .$adAction. ':edit', 'uses' =>'Admin\PagesController@edit']);
			Route::get('/{id}/active',['as'=> $asPrefix .$adAction. ':active', 'uses' =>'Admin\PagesController@active']);
		});
	});
});

