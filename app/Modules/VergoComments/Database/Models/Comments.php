<?php
namespace App\Modules\VergoComments\Database\Models;

use App\Modules\VergoBase\Database\Models\Base;

class Comments extends Base {
    public $TYPE = 'comments';
    public $timestamps = true;
    protected $table = 'comments';
    protected $perPage = 15;
    protected $fillable = array(
        'user_id',
        'article_id',
        'text',
        'status'
    );
    public function user(){
        return $this->hasOne('App\Modules\VergoBase\Database\Models\User','id','user_id');
    }

    public function article(){
        return $this->hasOne('App\Modules\VergoNews\Database\Models\ArticleModel','id','article_id');
    }

    public function getDate(){
        return date('H:i j M Y',strtotime($this->created_at));
    }
}
