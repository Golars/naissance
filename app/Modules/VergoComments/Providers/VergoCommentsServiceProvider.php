<?php
namespace App\Modules\VergoComments\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class VergoCommentsServiceProvider extends ServiceProvider
{
	/**
	 * Register the VergoComments module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\VergoComments\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the VergoComments module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('vergo_comments', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('vergo_comments', base_path('resources/views/vendor/vergo_comments'));
		View::addNamespace('vergo_comments', realpath(__DIR__.'/../Resources/Views'));
	}
}
