<?php
namespace App\Modules\VergoComments\Http\Controllers\Admin;

use App;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller {
    protected $prefix = 'admin:comments';
    protected $moduleName = 'vergo_comments';
    protected $serviceName = 'App\Modules\VergoComments\Http\Services\Comments';

    protected function prepareData($model = null){
        return [
            'model'     =>  (isset($model->id)) ? $model : $this->service->getModel(),
            'users'     =>  App::make('App\Modules\VergoBase\Http\Services\User')->prepareSelect(),
            'articles'  =>  App::make('App\Modules\VergoNews\Http\Services\Article')->prepareSelect()
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'user_id'       =>  'required',
            'article_id'    =>  'required',
            'text'          =>  'required|max:200'
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'            =>  'required',
            'user_id'       =>  'required',
            'article_id'    =>  'required',
            'text'          =>  'required|max:200'
        ]);
        return parent::edit($request, $id);
    }
}