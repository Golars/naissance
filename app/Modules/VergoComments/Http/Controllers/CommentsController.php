<?php
namespace App\Modules\VergoComments\Http\Controllers;

use App;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    protected $prefix = 'article';
    protected $moduleName = 'vergo_comments';
    protected $serviceName = 'App\Modules\VergoComments\Http\Services\Comments';

    public function add(Request $request)
    {
        $this->setRules([
            'user_id' => 'required',
            'article_id' => 'required',
            'text' => 'required|max:200'
        ]);
        if ($this->isValidationFails($request)){
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        if($this->service->getModel()->fill($this->getRulesInput($request))->save()){
            return $this->sendOk([
                'text'  =>  $this->service->getModel()->text,
                'cover' =>  $this->service->getModel()->user->getCover(),
                'name'  =>  $this->service->getModel()->user->getFullName(),
                'date'  =>  $this->service->getModel()->getDate()
            ]);
        }
        return $this->sendWithErrors('Something went wrong');
    }
}
