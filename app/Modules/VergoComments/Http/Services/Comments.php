<?php
namespace App\Modules\VergoComments\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class Comments extends Base
{
    protected $modelName = 'App\Modules\VergoComments\Database\Models\Comments';
}