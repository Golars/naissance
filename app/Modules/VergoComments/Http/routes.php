<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
		Route::group(['prefix' => 'comments'], function () use ($asPrefix) {
			$asAction = ':comments';
			Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\CommentsController@index']);
			Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\CommentsController@add']);
			Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\CommentsController@active']);
			Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\CommentsController@delete']);
			Route::any('/{id}/edit', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\CommentsController@edit']);
		});
	});
});
Route::group(['prefix' => 'site', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'site:article';
	Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
		Route::group(['prefix' => 'comments'], function () use ($asPrefix) {
			$asAction = ':comment';
			Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'CommentsController@add']);
		});
	});
});