@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Комментарии')

@section('form_body')
<link href="{{$app['vergo_news.assets']->getPath('/css/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

{!! Form::hidden('id', $model->id, ['class'=>'form-control'] ) !!}

<div class="form-group">
    {!! Form::label('Текст') !!}
    {!! Form::textarea('text', $model->text, ['class'=>'form-control','required'] ) !!}
</div>
<div class="form-group">
    {!! Form::label('Создатель') !!}
    {!! Form::select('user_id', $users, ($model->user_id) ? $model->user_id : Request::user()->id, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Новость') !!}
    {!! Form::select('article_id', $articles, $model->article_id, ['class'=>'form-control']) !!}
</div>
@endsection

@section('scripts')
@endsection