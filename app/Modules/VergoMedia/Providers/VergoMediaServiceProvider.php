<?php
namespace App\Modules\VergoMedia\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class VergoMediaServiceProvider extends ServiceProvider
{
	/**
	 * Register the VergoMedia module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\VergoMedia\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the VergoMedia module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('vergo_media', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('vergo_media', base_path('resources/views/vendor/vergo_media'));
		View::addNamespace('vergo_media', realpath(__DIR__.'/../Resources/Views'));
	}
}
