<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\VergoBase\Database\Models\Module;

class InitMedia extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Module::create([
			'name'				=> 'VergoMedia',
			'info' 				=> 'Vergo Media module foe Vergo Core',
			'version' 			=> 1,
			'install_version' 	=> 1,
			'status' 			=> 1
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}
}
