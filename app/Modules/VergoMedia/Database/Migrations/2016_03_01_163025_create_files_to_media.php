<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesToMedia extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files_to_media', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('files_id')->references('id')->on('files');
			$table->integer('media_id')->references('id')->on('media');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('files_to_media');
	}
}
