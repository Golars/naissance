<?php
namespace App\Modules\VergoMedia\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VergoMediaDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('App\Modules\VergoMedia\Database\Seeds\FoobarTableSeeder');
	}

}
