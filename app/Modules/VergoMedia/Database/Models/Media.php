<?php

namespace App\Modules\VergoMedia\Database\Models;

use App;
use App\Modules\VergoBase\Database\Models\Base;

class Media extends Base
{
    protected $table = 'media';
    public $timestamps = true;
    protected $perPage = 15;

    protected $fillable = array(
        'id',
        'status',
        'name',
        'cover_id'
    );

    public function files(){
        return $this->belongsToMany('App\Modules\VergoMedia\Database\Models\Image','files_to_media','media_id','files_id');
    }

    public function cover(){
        return $this->belongsTo('App\Modules\VergoMedia\Database\Models\Image');
    }

    public function setCoverIdAttribute($value){
        if($value){
            $this->attributes['cover_id'] = $value;
        }
    }

    public function getCover($w = null, $h = null){
        return ($this->cover) ? $this->cover->getCover($w,$h) : App::make('vergo_base.assets')->getPath('images/logo.png');
    }
}