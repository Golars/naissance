<?php

namespace App\Modules\VergoMedia\Database\Models;


use App\Modules\VergoBase\Database\Models\Base;

class FilesToMedia extends Base
{
    protected $table = 'files_to_media';
    public $timestamps = false;
    protected $fillable = array(
        'id',
        'files_id',//obj_id//delete
        'media_id',
    );

}