<?php
namespace App\Modules\VergoMedia\Database\Models;

use App\Modules\VergoBase\Database\Models\File;

class Image extends File
{
    protected $path = 'media/';
}
