@section('media_block')
    <span class="btn btn-default" id="spaner" style="display: {{($model->media_id) ? 'none' : 'block'}}"><i class="fa fa-plus"></i> Добавить Галерею</span>
    <div id="galery_panel" style="display: {{($model->media_id) ? 'block' : 'none' }}">
        <span class="btn btn-default btn-xs" style="position: absolute; right:0px" id="close_button"><i class="fa fa-close"></i> </span>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-plus-square"></i> Выбрать из существующих</a>
            </li>
            <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-pencil-square"></i> Добавить новую галерею</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="home">
                <div class="form-group">
                    {!! Form::label('') !!}
                    {!! Form::select('media_id', $media, $model->media_id, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile">
                <div class="col-xs-12 grid">
                </div>
                <div class="panel" id="addPhoto">
                    <span class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Добавить фото</span>
                    <div class="form-group">
                        {!! Form::file('file_photo', ['class' => 'hidden', 'id' => 'coverInput']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('media_script')
    <script>
        $('#spaner').on('click',function(){
            $(this).hide();
            $('#galery_panel').toggle(500);
        });
        $('#close_button').on('click',function(){
            $('#spaner').show();
            $('#galery_panel').hide(500);
            $('.elementPhoto').remove();
        });
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('#addPhoto span').on('click', function(){
            $('#coverInput').click();
            return;
        });
        $('#coverInput[type=file]').on('change', function(){
            PopUpShow();
            var formData = new FormData();
            if(this.files[0] == undefined) {
                return new PNotify({
                    title: 'Ошибка',
                    text: 'Файл не найден',
                    type: 'error'
                })
            }
            formData.append($(this).attr('name'), this.files[0]);
            var photo_element = _.template("<div id='el<%=id %>' class='elementPhoto col-md-2 col-sm-4 col-xs-6 panel grid-item'>"+
                    "<img src = '<%= image %>' class='img-responsive'>"+
                    "<span class='deletePhoto'><div class='middle'><i class='glyphicon glyphicon-trash'></i> Удалить фото</div></span>"+
                    "<input class='form-control' name='photo_ids[]' type='hidden' value='<%=id %>'></div>");
            $.ajax({
                type: 'POST',
                url: '/vadmin/media/add_photo',
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    PopUpHide();
                    var response = JSON.parse(res);
                    $item = photo_element({image:response.image,id:response.id});
                    console.log($item);
                    $('.grid').append($item);
                    $('#el'+response.id).find('.deletePhoto').on('click', function(){
                        deletePhoto(this);
                    });
                    new PNotify({
                        title: 'Успех',
                        text: 'Добавление данных прошло успешно!',
                        type: 'success'
                    });
                },
                error: function (res) {
                    PopUpHide();
                    new PNotify({
                        title: 'Ошибка',
                        text: 'Фото не загружено',
                        type: 'error'
                    });
                    return false;
                }
            });
        });
        var deletePhoto = function(self) {
            var rootEl = $(self).parent();
            var id = rootEl.find('input').val();

            $.ajax({
                type: 'DELETE',
                url: '/vadmin/media/del_photo/' + id,
                success: function(res) {
                    new PNotify({
                        title: 'Успех',
                        text: 'Удаление данных прошло успешно!',
                        type: 'success'
                    });
                    rootEl.slideToggle(500);
                    setTimeout(function(){
                        rootEl.remove();
                    }, 500);
                },
                error: function(res){
                    var errors = JSON.parse(res.responseText).errors || ['Данные не валидные'];
                    new PNotify({
                        title: 'Ошибка',
                        text: errors.join("<br>"),
                        type: 'error'
                    });
                    return false;
                }
            });
        };
        $('.elementPhoto .deletePhoto').on('click', function(e){
            deletePhoto(this);
        });
    </script>
@endsection