@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Медиа')

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label('Обложка') !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> Выбрать обложку</div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{($model->cover) ? $model->cover->getCover() : $app['logo']}}" width="250" class="img-responsive">
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('Название') !!}
        {!! Form::text('name',$model->name, ['class'=>'form-control']) !!}
    </div>
    <div class="col-xs-12 grid">
        @foreach($model->files as $file)
            <div id='el{{$file->id}}' class='elementPhoto col-md-2 col-sm-4 col-xs-6 panel grid-item'>
                <img src = '{{$file->getCover()}}' class='img-responsive'>
                <span class='deletePhoto'>
                    <div class='middle'>
                        <i class='glyphicon glyphicon-trash'></i> Удалить фото
                    </div>
                </span>
                <input class='form-control' name='photo_ids[]' type='hidden' value='{{$file->id}}'>
            </div>
        @endforeach
    </div>
    <div class="panel" id="addPhoto">
        <span class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Добавить фото</span>
        <div class="form-group">
            {!! Form::file('file_photo', ['class' => 'hidden', 'id' => 'coverInput']) !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{$app['vergo_base.assets']->getPath('js/lib/underscore/underscore-min.js')}}"></script>
    <script>
        $('#addPhoto span').on('click', function(){
            $('#coverInput').click();
            return;
        });
        $('#coverInput[type=file]').on('change', function(){
            PopUpShow();
            var formData = new FormData();
            if(this.files[0] == undefined) {
                return new PNotify({
                    title: 'Ошибка',
                    text: 'Файл не найден',
                    type: 'error'
                })
            }
            formData.append($(this).attr('name'), this.files[0]);
            var photo_element = _.template("<div id='el<%=id %>' class='elementPhoto col-md-2 col-sm-4 col-xs-6 panel grid-item'>"+
                    "<img src = '<%= image %>' class='img-responsive'>"+
                    "<span class='deletePhoto'><div class='middle'><i class='glyphicon glyphicon-trash'></i> Удалить фото</div></span>"+
                    "<input class='form-control' name='photo_ids[]' type='hidden' value='<%=id %>'></div>");
            $.ajax({
                type: 'POST',
                url: '/vadmin/media/add_photo',
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    PopUpHide();
                    var response = JSON.parse(res);
                    $item = photo_element({image:response.image,id:response.id});
                    console.log($item);
                    $('.grid').append($item);
                    $('#el'+response.id).find('.deletePhoto').on('click', function(){
                        deletePhoto(this);
                    });
                    new PNotify({
                        title: 'Успех',
                        text: 'Добавление данных прошло успешно!',
                        type: 'success'
                    });
                },
                error: function (res) {
                    PopUpHide();
                    new PNotify({
                        title: 'Ошибка',
                        text: 'Фото не загружено',
                        type: 'error'
                    });
                    return false;
                }
            });
        });

        var deletePhoto = function(self) {
            var rootEl = $(self).parent();
            var id = rootEl.find('input').val();

            $.ajax({
                type: 'DELETE',
                url: '/vadmin/media/del_photo/' + id,
                success: function(res) {
                    new PNotify({
                        title: 'Успех',
                        text: 'Удаление данных прошло успешно!',
                        type: 'success'
                    });
                    rootEl.slideToggle(500);
                    setTimeout(function(){
                        rootEl.remove();
                    }, 500);
                },
                error: function(res){
                    var errors = JSON.parse(res.responseText).errors || ['Данные не валидные'];
                    new PNotify({
                        title: 'Ошибка',
                        text: errors.join("<br>"),
                        type: 'error'
                    });
                    return false;
                }
            });
        };

        $('.elementPhoto .deletePhoto').on('click', function(e){
            deletePhoto(this);
        });
    </script>
@endsection