@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Медиа<small>управление мультимедиа</small>')

@section('tHead')
    <th>Название</th>
    <th>Обложка</th>
    <th>Количество вложений</th>
    <th>Дата создания</th>
@endsection
@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}</td>
            <td>
                <img src="{{($model->cover) ? $model->cover->getCover() : $app['logo']}}" width="75" class="img-responsive">
            </td>
            <td>
                {{$model->files->count()}}
            </td>
            <td>
                {{$model->created_at}}
            </td>
            <td class="last">
                <a class="btn btn-success" href="{{route('admin:media:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:media:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection