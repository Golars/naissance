@if($model->media)
<div class="image col-xs-12">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="min-height: 405px">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <?php $i = 0;?>
        @foreach($model->media->files as $file)
            <?php $i++;?>
            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}"></li>
        @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="{{$model->media->getCover()}}" />
        </div>
        @foreach($model->media->files as $file)
            <div class="item">
                <img src="{{$file->getCover()}}" />
            </div>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
@endif