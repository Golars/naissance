<?php
namespace App\Modules\VergoMedia\Http\Controllers\Admin;

use App;
use App\Modules\VergoBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class MediaController extends Controller {
    protected $prefix = 'admin:media';
    protected $moduleName = 'vergo_media';
    protected $serviceName = 'App\Modules\VergoMedia\Http\Services\Media';

    protected function prepareData($model = null){
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel()
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'name'      =>  'required|min:2|max:255',
            'photo_ids' =>  'required',
            'cover_id'  => '',
        ]);
        if($request->hasFile('cover')) {
            $imageId = $this->saveCover($request);
            $request->merge(array('cover_id' => $imageId));
        }
        /**
         * Creating service->model for model's id.
         */
        $result = parent::add($request);
        if (isset($this->service->getModel()->id)) {
            $this->saveMedia($request, $this->service->getModel()->id);
        }
        return $result;
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'    =>  'required',
            'name'  =>  'required|min:2|max:255',
            'photo_ids' =>  'required',
            'cover_id'  => '',
        ]);
        $this->saveMedia($request,$id);
        if($request->hasFile('cover')) {
            $imageId = $this->saveCover($request);
            $request->merge(array('cover_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    /**
     * Save Media Cover
     * @param Request $request
     * @return mixed
     */
    private function saveCover(Request $request){
        $service = new App\Modules\VergoMedia\Http\Services\Image($request->file('cover'));
        $service->saveFile($request->user()->id . 'cover');
        return $service->getModel()->id;
    }

    /**
     * Save Files Into Relation Table
     * @param Request $request
     * @param $id
     */
    private function saveMedia(Request $request,$id){
        if(isset($request['photo_ids'])) {
            foreach($request['photo_ids'] as $key=>$file_id){
                $relation = new App\Modules\VergoMedia\Http\Services\ImagesRelation();
                $model = $relation->getModel();
                $relation->setWhere(['files_id'=>$file_id]);
                if (!isset($relation->getOne()->id)) {
                    $model->create([
                        'files_id' => $file_id,
                        'media_id' => $id
                    ]);
                }
            }
        }
    }

    /**
     * Add Photo file from AJAX
     * @param Request $request
     * @return App\Modules\VergoBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function addPhoto(Request $request){
        if (!$request->hasFile('file_photo')) {
            return $this->sendWithErrors('Image is not found', 404);
        }
        $image = $this->saveFile($request);
        return $this->sendOk([
            'image' => $image->getCover(),
            'id'=> $image->id
        ]);
    }

    /**
     * Save File from AJAX request
     * @param Request $request
     * @return App\Modules\VergoBase\Database\Models\Base
     */
    private function saveFile(Request $request){
        $service = new App\Modules\VergoMedia\Http\Services\Image($request->file('file_photo'));
        $service->saveFile($request->user()->id);
        return $service->getModel();
    }

    /**
     * Delete File from AJAX
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function deletePhoto(Request $request,$id){
        if(!$id){
            return $this->sendWithErrors('Id is incorect', 404);
        }
        $service = new App\Modules\VergoNews\Http\Services\Image();
        $model = $service->getOne($id);
        if (!$model->id){
            return $this->sendWithErrors('Not found file',404);
        }
        $relation = new App\Modules\VergoMedia\Http\Services\ImagesRelation();
        $relation->setWhere(['files_id'=>$model->id]);
        $relation->getOne()->delete();
        $model->delete();
    }
}