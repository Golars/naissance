<?php
namespace App\Modules\VergoMedia\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;

class ImagesRelation extends Base
{
    protected $modelName = 'App\Modules\VergoMedia\Database\Models\FilesToMedia';
    protected $orderBy = [];
    protected $hasStatus = false;

}