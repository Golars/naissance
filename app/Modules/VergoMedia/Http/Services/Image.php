<?php
namespace App\Modules\VergoMedia\Http\Services;

use App\Modules\VergoBase\Http\Services\Files;

class Image extends Files
{
    protected $modelName = 'App\Modules\VergoMedia\Database\Models\Image';
    protected $orderBy = [];
    protected $hasStatus = false;
}