<?php
namespace App\Modules\VergoMedia\Http\Services;

use App\Modules\VergoBase\Http\Services\Base;
use App\Modules\VergoNews\Database\Models\ArticleModel;
use App\Modules\VergoNews\Http\Services\Article;

class Media extends Base
{
    protected $modelName = 'App\Modules\VergoMedia\Database\Models\Media';
    protected $with = ['files'];

    public function prepareSelect(){
        $ids = ArticleModel::query()
            ->distinct()
            ->lists('media_id')
            ->toArray();

        $media = [];
        foreach($this->getModel()->whereNotIn('id',$ids)->get() as $unit){
            $media[$unit->id] = $unit->name;
        }
        return $media;
    }

}