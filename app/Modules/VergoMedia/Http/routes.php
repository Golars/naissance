<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => 'vadmin', 'middleware' => ['webAdmin']], function() {
	$asPrefix = 'admin';
	Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
		Route::group(['prefix' => 'media'], function () use ($asPrefix) {
			$asAction = ':media';
			Route::get('/', ['as' => $asPrefix . $asAction . ':index', 'uses' => 'Admin\MediaController@index']);
			Route::any('/add', ['as' => $asPrefix . $asAction . ':add', 'uses' => 'Admin\MediaController@add']);
			Route::get('/{id}/active', ['as' => $asPrefix . $asAction . ':active', 'uses' => 'Admin\MediaController@active']);
			Route::any('{id}/delete', ['as' => $asPrefix . $asAction . ':delete', 'uses' => 'Admin\MediaController@delete']);
			Route::any('/{id}/edit', ['as' => $asPrefix . $asAction . ':edit', 'uses' => 'Admin\MediaController@edit']);
			Route::post('/add_photo',['as' => $asPrefix . $asAction . ':add:photo', 'uses' => 'Admin\MediaController@addPhoto']);
			Route::delete('/del_photo/{id}',['as' => $asPrefix . $asAction . ':dalete:photo', 'uses' => 'Admin\MediaController@deletePhoto']);
		});
	});
});
