@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Роли')

@section('form_body')
    <div class="form-group">
        {!! Form::label('Название') !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Возможность администрирования') !!}
        {!! Form::checkbox('is_admin', $model->is_admin, $model->is_admin) !!}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
@endsection