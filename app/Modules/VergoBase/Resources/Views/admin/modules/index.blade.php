@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Модуль <small>управление модулями</small>')

@section('tHead')
    <th>Название</th> <!-- тут бы поправить чутка -->
    <th>Краткая информация</th>
    <th>Версия</th>
    <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->version > $model->install_version) ? "info" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}</td>
            <td>{{$model->info}}</td>
            <td>{{$model->version}}</td>
            <td>
                <span class="{{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-danger" href="#">
                    <i class="fa fa-remove"></i>
                </a>
                <a class="btn btn-success" href="#">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection


