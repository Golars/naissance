@extends('vergo_base::admin.layouts.default')

{{-- Content --}}
@section('content')
    <div class="">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">{{$media}}</div>

                    <h3>Медиа</h3>
                    <p>Новые галлереи</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i>
                    </div>
                    <div class="count">{{$comments}}</div>

                    <h3>Комментарии</h3>
                    <p>Новые комментарии</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                    </div>
                    <div class="count">{{$users}}</div>

                    <h3>Пользователи</h3>
                    <p>Новые пользователи</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="count">{{$news}}</div>

                    <h3>Новости</h3>
                    <p>Новые новости</p>
                </div>
            </div>
        </div>
        <div class="row">

            <!-- form input knob -->
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    @include('vergo_base::admin.layouts.block.top_menu')
                </div>
            </div>
            <!-- /form input knob -->

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Новые новости</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="{{$getRoute('add')}}"><i class="fa fa-plus"></i></a>
                        </li>
                        <li>
                            <a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @foreach($newest as $new)
                        <article class="media event">
                        <a class="pull-left date">
                            <p class="month">{{$new->getDate('M')}}</p>
                            <p class="day">{{$new->getDate('j')}}</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="{{$getRoute('edit',$new->id)}}">{{$new->name}}</a>
                            <p>{{$new->info}}</p>
                        </div>
                    </article>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Самые популярные</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @foreach($popular as $new)
                        <article class="media event">
                        <a class="pull-left date">
                            <p class="month">{{$new->getDate('M')}}</p>
                            <p class="day">{{$new->getDate('j')}}</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="{{$getRoute('edit',$new->id)}}">{{$new->name}}</a>
                            <p>{{$new->info}}</p>
                        </div>
                    </article>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Не провереные</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @foreach($unchecked as $new)
                        <article class="media event">
                        <a class="pull-left date">
                            <p class="month">{{$new->getDate('M')}}</p>
                            <p class="day">{{$new->getDate('j')}}</p>
                        </a>
                        <div class="media-body">
                            <a class="title" href="{{$getRoute('edit',$new->id)}}">{{$new->name}}</a>
                            <p>{{$new->info}}</p>
                        </div>
                    </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop