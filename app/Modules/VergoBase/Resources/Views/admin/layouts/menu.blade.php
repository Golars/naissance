
<!-- sidebar menu -->

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

	<div class="menu_section">
		<ul class="nav side-menu" style="padding-top: 80px;">
			<li>
				<a href="{{route('admin:index')}}"><i class="fa fa-home"></i> {{trans('vergo_base::main.menu_main')}}</a>
			</li>
			<li>
				<a>
					<i class="fa fa-newspaper-o"></i> Новости<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:news:index')}}">Новости</a>
						<a href="{{route('admin:news:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:categories:index')}}">Категории</a>
						<a href="{{route('admin:categories:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:comments:index')}}">Комментарии</a>
						<a href="{{route('admin:comments:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{route('admin:media:index')}}"><i class="fa fa-photo"></i> Медиа</a>
				<a href="{{route('admin:media:add')}}" class="pull-right create fa fa-plus-circle"></a>
			</li>
			<li>
				<a>
					<i class="fa fa-users"></i> Пользователи<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:users:index')}}">Пользователи</a>
						<a href="{{route('admin:users:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:roles:index')}}">Роли</a>
						<a href="{{route('admin:roles:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{route('admin:static:index')}}"><i class="fa fa-files-o"></i> Веб Страница
					<a href="{{route('admin:static:add')}}" class="pull-right create fa fa-plus-circle"></a>
				</a>
			</li>
			<li>
				<a>
					<i class="fa fa-envelope"></i> Письма<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:templates:index')}}">Шаблоны</a>
						<a href="{{route('admin:templates:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:scenarios:index')}}">Сценарии</a>
						<a href="{{route('admin:scenarios:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li>
						<a href="{{route('admin:mails:index')}}">Отчеты</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{route('admin:banners:index')}}"><i class="fa fa-ban"></i> Баннеры
					<a href="{{route('admin:banners:add')}}" class="pull-right create fa fa-plus-circle"></a>
				</a>
			</li>
			<li>
			<li>
				<a>
					<i class="fa fa-gear"></i> Система<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:language:index')}}">Языки</a>
						<a href="{{route('admin:language:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:modules:index')}}"><i class="fa fa-power-off"></i> Модули</a></li>
				</ul>
			</li>
			</li>
		</ul>
	</div>
</div>
<!-- /sidebar menu -->