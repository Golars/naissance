@extends('vergo_base::admin.layouts.all')

@section('page_title', 'Пользователи <small>управление пользователями</small>')

@section('tHead')
    <th>Логин</th>
    <th>Имя пользователя</th>
    <th>E-Mail</th>
    <th>Роль</th>
    <th>Аватар</th>
    <th>Статус</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->login}}</td>
            <td>{{$model->getFullName()}}</td>
            <td>{{$model->email}}</td>
            <td>
                {{$model->getRole()->name}}
            </td>
            <td>
                <img src="{{$model->getCover()}}" width="75">
            </td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-success" href="{{$getRoute('edit', ['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}" href="{{$getRoute('active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                @if($model->status != $model::STATUS_ACTIVE)
                    <a class="btn btn-danger" href="{{$getRoute('delete', ['id'=>$model->id])}}">
                        <i class="fa fa-trash"></i>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
@endsection


