@extends('vergo_base::admin.layouts.edit')

@section('title_name', 'Пользователя')

@section('form_body')
    <div class="form-group">
        {!! Form::label('Аватар') !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> Выбрать обложку</div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
        {{--{!! Form::text('file_cover_id', $model->file_cover_id, ['class'=>'form-control hidden'] ) !!}--}}
    </div>

    <div class="form-group">
        {!! Form::label('Логин') !!}
        {!! Form::text('login', $model->login, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Имя') !!}
        {!! Form::text('first_name', $model->first_name, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Фамилия') !!}
        {!! Form::text('last_name', $model->last_name, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('E-Mail адресс') !!}
        {!! Form::text('email', $model->email, ['class'=>'form-control','required'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Роль') !!}
        {!! Form::select('role_id', $roles,$model->role_id, ['class'=>'form-control'] ) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Пароль') !!}
        {!! Form::password('password', '', ['class'=>'form-control'] ) !!}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
@endsection