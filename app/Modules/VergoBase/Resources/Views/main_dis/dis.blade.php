<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name=description content="">
    <meta name=keywords content="">
    <meta name=author content="">
    <title>Dis</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/vergo.css">
    <link rel="stylesheet" href="/fonts/css/font-awesome.min.css">
    <script src="/js/lib/jquery/jquery-2.1.3.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/npm.js"></script>
    <script src="/js/lib/masonry/masonry.pkgd.min.js"></script>
    <script src="/js/lib/masonry/imagesloaded.pkgd.min.js"></script>
</head>
<body>
<!-- Section Header -->
<section id="header" class="container">
    <div class="row">
        <div class="header">
            <div class="title">
                <div class="upper">Во что, вкладывает билл?</div>
                <div class="smaller">Читай на КО*</div>
            </div>
            <div class="button">
                <a href="#">
                    <span class="btn btn-default btn-lg btn-rounded">Узнать больше</span>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Section Main -->
<section id="main" class="container">
    <div class="row menu-set">
        <div class="col-md-3 unpadding">
            <img src="/img/yeap.png" class="img-responsive">
        </div>
        <div class="col-md-9">
            <div class="col-xs-6 unpadding">
                <div class="col-xs-3 unpadding small">22 Декабря</div>
                <div class="col-xs-3 unpadding small"><i class="fa fa-cloud green"></i> Ташкент</div>
                <div class="col-xs-6 unpadding small">+4..+6 дождь</div>
            </div>
            <div class="col-xs-6">
                <div class="col-xs-7 small text-right">
                    <a href="#">
                        <i class="fa big">+</i> Отправить материал
                    </a>
                </div>
                <div class="col-xs-3 unpadding small">
                    <a href="#">
                        На русском
                    </a>
                </div>
                <div class="col-xs-2 unpadding small">
                    <a href="#">
                        Подписка
                    </a>
                </div>
            </div>
            <div class="col-xs-12 unpadding menu-list">
                <ul class="list-inline">
                    <li class="text-uppercase">
                        <a href="#">
                            Наука
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Экономика
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Бизнес
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#" class="small">
                            Мода
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Культура
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Видео
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Мода
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <span class="btn btn-default btn-rounded">Войти</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <span class="btn btn-default btn-rounded">Регистрация</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="grid">
            {{--popular--}}
            <div class="grid-item col-sm-3">
                <div class="news_right_block">
                    <div class="col-sm-12 unpadding article-header">
                        <div class="col-md-6 small sans">Популярное</div>
                        <div class="col-md-6 green b text-right">Просмотреть все</div>
                    </div>
                    <!-- Элементы foreach -->
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Культура
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        На сцене, в кадре жизни:
                                        Все о Михаиле Боярском
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Кино
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся под ногами
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Еда
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Куда пойти в выходные с ребенком что бы не было.
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Интервью
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся по ногами
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <!-- endforeach -->
                </div>
            </div>
            {{--video--}}
            <div class="grid-item col-sm-6 panel">
                <a class="el-image">
                    <img src="/img/video.png" class="img-responsive" >
                    {{--<video class="img-responsive unpadding" controls="controls">--}}
                    {{--<source src="https://cs542304.vk.me/7/u12534244/videos/6401c47879.720.mp4">--}}
                    {{--</video>--}}
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-7 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-5 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div class="col-xs-12">
                            <p class="text">Бывший сотрудник "Яндекса" осужден за попытку продажи поискового кода. Украден код "Яндекса"</p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/1.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        <i class="fa fa-eye el-type"></i>
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/6.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            {{--custom image--}}
            <div class="grid-item col-sm-3 panel">
                <a class="el-image">
                    <img src="/img/8.jpg" class="img-responsive" >
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </div>
                </a>
            </div>
            {{--custom image vers2--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/7.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                    <div class="col-xs-12">
                        <p class="text">
                            Независимо от поры года крысы спят на подушках из слонячего и
                            пускают слюнки на маленькие косточки гигантского крокозябра
                        </p>
                        <span class="article-foot-text">Знаменитому актеру</span>
                    </div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/5.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info vers2-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        {{--<i class="fa fa-eye el-type"></i>--}}
                        <img src="/img/ava-a-a.jpg" class="img-responsive img-circle image-info">
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        <i class="fa fa-eye el-type"></i>
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            <!-- info vers2-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        {{--<i class="fa fa-eye el-type"></i>--}}
                        <img src="/img/ava-a-a.jpg" class="img-responsive img-circle image-info">
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            {{--custom image vers2--}}
            <div class="grid-item col-sm-6 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/3.png" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                    <div class="col-xs-12">
                        <p class="text">
                            Независимо от поры года крысы спят на подушках из слонячего и
                            пускают слюнки на маленькие косточки гигантского крокозябра
                        </p>
                        <span class="article-foot-text">Знаменитому актеру</span>
                    </div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/3.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            {{--custom image vers2--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/1.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                    <div class="col-xs-12">
                        <p class="text">
                            Независимо от поры года крысы спят на подушках из слонячего и
                            пускают слюнки на маленькие косточки гигантского крокозябра
                        </p>
                        <span class="article-foot-text">Знаменитому актеру</span>
                    </div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        <i class="fa fa-eye el-type"></i>
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        <i class="fa fa-eye el-type"></i>
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            <!-- info vers2-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        {{--<i class="fa fa-eye el-type"></i>--}}
                        <img src="/img/ava-a-a.jpg" class="img-responsive img-circle image-info">
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            {{--popular--}}
            <div class="grid-item col-sm-3">
                <div class="news_right_block">
                    <div class="col-sm-12 unpadding article-header">
                        <div class="col-md-6 small sans">Популярное</div>
                        <div class="col-md-6 green b text-right">Просмотреть все</div>
                    </div>
                    <!-- Элементы foreach -->
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Культура
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        На сцене, в кадре жизни:
                                        Все о Михаиле Боярском
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Кино
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся под ногами
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Еда
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Куда пойти в выходные с ребенком что бы не было.
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <div class="col-sm-12 article-body">
                        <a href="#" class="el">
                            <div class="col-md-12 unpadding title_of_article">
                                <div class="col-md-7 unpadding green text-uppercase a">
                                    Интервью
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding a text-muted">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding a text-muted">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся по ногами
                                    </span>
                            <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                            <hr/>
                        </a>
                    </div>
                    <!-- endforeach -->
                </div>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/1.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
            <!-- info-->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-3 unpadding">
                        <i class="fa fa-eye el-type"></i>
                    </div>
                    <div class="col-md-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/6.jpg" class="img-responsive">
                    <div class="col-md-12 unpadding title-image">
                        <div class="col-md-7 small  text-uppercase">
                            Культура
                        </div>
                        <div class="col-md-5 unpadding">
                            <div class="col-md-7 unpadding a">
                                <i class="fa fa-eye"> 52876</i>
                            </div>
                            <div class="col-md-5 unpadding a">
                                <i class="fa fa-comment-o"> 23</i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            {{--custom image--}}
            <div class="grid-item col-sm-3 panel">
                <a class="el-image">
                    <img src="/img/8.jpg" class="img-responsive" >
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr/>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </div>
                </a>
            </div>
            {{--custom image vers2--}}
            <div class="grid-item col-sm-3 panel">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/7.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                    <div class="col-xs-12">
                        <p class="text">
                            Независимо от поры года крысы спят на подушках из слонячего и
                            пускают слюнки на маленькие косточки гигантского крокозябра
                        </p>
                        <span class="article-foot-text">Знаменитому актеру</span>
                    </div>
                </a>
            </div>
            {{--image--}}
            <div class="grid-item col-sm-3">
                <a href="#" class="el unpadding el-image">
                    <img src="/img/5.jpg" class="img-responsive">
                    <div class="col-md-12 title-image alternative">
                        <div class="col-md-12 title_of_article">
                            <div class="col-md-6 unpadding text-uppercase">
                                Кино
                            </div>
                            <div class="col-md-6 unpadding text-right">
                                <div class="col-md-7 unpadding">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- comment -->
            <div class="grid-item col-sm-3">
                <a href="#" class="el panel panel-body">
                    <div class="col-md-12 unpadding title_of_article">
                        <div class="col-md-2 col-xs-4 unpadding">
                            <img src="/img/ava.jpg" class="img-responsive img-circle" width="50px">
                        </div>
                        <div class="col-md-3 col-xs-8 io">
                            Alexander
                        </div>
                        <div class="col-md-7 unpadding green small text-right io">
                            17 Дек. 2016
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="name text comment-text">
                        На сцене, в кадре жизни:
                        Все о Михаиле Боярском
                    </div>
                    <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                    <hr/>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Section Bottom menu -->
<section id="bottom-menu" class="container">
    <div class="row menu-set">
        <div class="col-md-2">
            <img src="/img/yeap.png" class="img-responsive">
        </div>
        <div class="col-md-7">
            <div class="col-xs-12 unpadding menu-list-alt">
                <ul class="list-inline">
                    <li class="text-uppercase">
                        <a href="#">
                            Наука
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Экономика
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Бизнес
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#" class="small">
                            Мода
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Культура
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Видео
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Мода
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 menu-list-alt">
            <div class="col-md-6">
                <a href="#">
                    <img class="img-responsive border-lank" src="/img/apple.png">
                </a>
            </div>
            <div class="col-md-6">
                <a href="#">
                    <img class="img-responsive border-lank" src="/img/play.png">
                </a>
            </div>
        </div>
    </div>
</section>
{{--Section Footer--}}
<section id="footer" class="container">
    <div class="row">
        <div class="col-md-2 col-sm-6 unpadding">
            <div class="col-md-12"><p class="foot-title">Подписка</p></div>
            <div class="col-md-12 foot-menu unpadding">
                <div class="col-md-6 a">
                    <a href="#">
                        Для вас
                    </a>
                </div>
                <div class="col-md-6 a">
                    <a href="#">
                        Подписка+
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 unpadding">
            <div class="col-md-12"><p class="foot-title">Сервисы</p></div>
            <div class="col-md-12 foot-menu unpadding">
                <div class="col-md-7">
                    <a href="#">
                        Отправить материал
                    </a>
                </div>
                <div class="col-md-5">
                    <a href="#">
                        Колумнисты
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-6 unpadding">
            <div class="col-md-12"><p class="foot-title">Реклама</p></div>
            <div class="col-md-12 foot-menu unpadding">
                <div class="col-md-6">
                    <a href="#">
                        Баннеры
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="#">
                        Статьи
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-5 text-right col-sm-6 col-sm-offset-0 unpadding foot-menu">
            <div class="col-md-9">
                <ul class="list-inline">
                    <li class="add-padding">
                        <a href="">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="add-padding">
                        <a href="">
                            <i class="fa fa-vk"></i>
                        </a>
                    </li>
                    <li class="add-padding">
                        <a href="">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <span class="circle age16">16+</span>
            </div>
        </div>
    </div>
</section>
</body>
<script>
    $(document).ready(function(){
        $('.title-image').hide();
    })
    // init Masonry
    var $grid = $('.grid').masonry({
        // options...
    });
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
        $grid.masonry('layout');
    });
    $('.el-image').on('mousemove',function(){
//        $('.title-image').removeClass('hidden')
        $(this).find('.title-image').fadeIn(300);
//        $('.title-image').fadeIn(300);
    })
    $('.el-image').on('mouseleave',function($this){
//        $('.title-image').addClass('hidden')
        $(this).find('.title-image').fadeOut(300);
//        $('.title-image').fadeOut(300);
    })
</script>
</html>