@extends('vergo_base::site.layouts.main')

@section('head_panel')
    <section id="header" class="container">
        <div class="row">
            <div class="header">
                <div class="title">
                    <div class="upper">Во что, вкладывает билл?</div>
                    <div class="smaller">Читай на КО*</div>
                </div>
                <div class="button">
                    <a href="#">
                        <span class="btn btn-default btn-lg btn-rounded">Узнать больше</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section id="main" class="container">
        <div class="row">
            <div class="grid">
                {{--popular--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="news_right_block">
                        <div class="col-sm-12 unpadding article-header">
                            <div class="col-md-6 small sans">Популярное</div>
                            <div class="col-md-6 green b text-right">Просмотреть все</div>
                        </div>
                        <!-- Элементы foreach -->
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Культура
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        На сцене, в кадре жизни:
                                        Все о Михаиле Боярском
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Кино
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся под ногами
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Еда
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Куда пойти в выходные с ребенком что бы не было.
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Интервью
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся по ногами
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <!-- endforeach -->
                    </div>
                </div>
                {{--video--}}
                <div class="grid-item col-sm-6 panel">
                    <a class="el-image">
                        <img src="{{$app['logo']}}" class="img-responsive" >
                        {{--<video class="img-responsive unpadding" controls="controls">--}}
                        {{--<source src="https://cs542304.vk.me/7/u12534244/videos/6401c47879.720.mp4">--}}
                        {{--</video>--}}
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-7 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-5 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="col-xs-12">
                                <p class="text">Бывший сотрудник "Яндекса" осужден за попытку продажи поискового кода. Украден код "Яндекса"</p>
                                <span class="article-foot-text">Знаменитому актеру</span>
                            </div>
                        </div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-xs-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-xs-6 unpadding text-right">
                                    <div class="col-xs-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-xs-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            <i class="fa fa-eye el-type"></i>
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                {{--custom image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a class="el-image">
                        <img src="{{$app['logo']}}" class="img-responsive" >
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                            <div class="col-xs-12">
                                <p class="text">
                                    Независимо от поры года крысы спят на подушках из слонячего и
                                    пускают слюнки на маленькие косточки гигантского крокозябра
                                </p>
                                <span class="article-foot-text">Знаменитому актеру</span>
                            </div>
                        </div>
                    </a>
                </div>
                {{--custom image vers2--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info vers2-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            {{--<i class="fa fa-eye el-type"></i>--}}
                            <img src="{{$app['logo']}}" class="img-responsive img-circle image-info">
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            <i class="fa fa-eye el-type"></i>
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                <!-- info vers2-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            {{--<i class="fa fa-eye el-type"></i>--}}
                            <img src="{{$app['logo']}}" class="img-responsive img-circle image-info">
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                {{--custom image vers2--}}
                <div class="grid-item col-sm-6 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                {{--custom image vers2--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            <i class="fa fa-eye el-type"></i>
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            <i class="fa fa-eye el-type"></i>
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                <!-- info vers2-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            {{--<i class="fa fa-eye el-type"></i>--}}
                            <img src="{{$app['logo']}}" class="img-responsive img-circle image-info">
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                {{--popular--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="news_right_block">
                        <div class="col-sm-12 unpadding article-header">
                            <div class="col-md-6 small sans">Популярное</div>
                            <div class="col-md-6 green b text-right">Просмотреть все</div>
                        </div>
                        <!-- Элементы foreach -->
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Культура
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        На сцене, в кадре жизни:
                                        Все о Михаиле Боярском
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Кино
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся под ногами
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Еда
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Куда пойти в выходные с ребенком что бы не было.
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <div class="col-sm-12 article-body">
                            <a href="#" class="el">
                                <div class="col-md-12 unpadding title_of_article">
                                    <div class="col-md-7 unpadding green text-uppercase a">
                                        Интервью
                                    </div>
                                    <div class="col-md-5 unpadding text-right">
                                        <div class="col-md-7 unpadding a text-muted">
                                            <i class="fa fa-eye"> 52876</i>
                                        </div>
                                        <div class="col-md-5 unpadding a text-muted">
                                            <i class="fa fa-comment-o"> 23</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <span class="name text">
                                        Кажеться весь мир перевернулся по ногами
                                    </span>
                                <span class="footer-article">Леонардо Ди Каприо, о Гесби.</span>
                                <hr/>
                            </a>
                        </div>
                        <!-- endforeach -->
                    </div>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
                <!-- info-->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-xs-3 unpadding">
                            <i class="fa fa-eye el-type"></i>
                        </div>
                        <div class="col-xs-9 unpadding">New Horizon сняло движение обьектов на поясе Картера</div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 unpadding title-image">
                            <div class="col-md-7 small  text-uppercase">
                                Культура
                            </div>
                            <div class="col-md-5 unpadding">
                                <div class="col-md-7 unpadding a">
                                    <i class="fa fa-eye"> 52876</i>
                                </div>
                                <div class="col-md-5 unpadding a">
                                    <i class="fa fa-comment-o"> 23</i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                {{--custom image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a class="el-image">
                        <img src="{{$app['logo']}}" class="img-responsive" >
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr/>
                            <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                            <div class="col-xs-12">
                                <p class="text">
                                    Независимо от поры года крысы спят на подушках из слонячего и
                                    пускают слюнки на маленькие косточки гигантского крокозябра
                                </p>
                                <span class="article-foot-text">Знаменитому актеру</span>
                            </div>
                        </div>
                    </a>
                </div>
                {{--custom image vers2--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12 panel">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 green dtn">17 Дек. 2016</div>
                        <div class="col-xs-12">
                            <p class="text">
                                Независимо от поры года крысы спят на подушках из слонячего и
                                пускают слюнки на маленькие косточки гигантского крокозябра
                            </p>
                            <span class="article-foot-text">Знаменитому актеру</span>
                        </div>
                    </a>
                </div>
                {{--image--}}
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el unpadding el-image">
                        <img src="{{$app['logo']}}" class="img-responsive">
                        <div class="col-md-12 title-image alternative">
                            <div class="col-md-12 title_of_article">
                                <div class="col-md-6 unpadding text-uppercase">
                                    Кино
                                </div>
                                <div class="col-md-6 unpadding text-right">
                                    <div class="col-md-7 unpadding">
                                        <i class="fa fa-eye"> 52876</i>
                                    </div>
                                    <div class="col-md-5 unpadding">
                                        <i class="fa fa-comment-o"> 23</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- comment -->
                <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="el panel panel-body">
                        <div class="col-md-12 unpadding title_of_article">
                            <div class="col-md-2 col-xs-4 unpadding">
                                <img src="{{$app['logo']}}" class="img-responsive img-circle" width="50px">
                            </div>
                            <div class="col-md-3 col-xs-8 io">
                                Alexander
                            </div>
                            <div class="col-md-7 unpadding green small text-right io">
                                17 Дек. 2016
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="name text comment-text">
                            На сцене, в кадре жизни:
                            Все о Михаиле Боярском
                        </div>
                        <div class="col-xs-12 unpadding">Знаменитому актёру</div>
                        <hr/>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection