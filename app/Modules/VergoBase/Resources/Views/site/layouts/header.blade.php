<section id="main" class="container">
    <div class="row menu-set">
        <div class="col-md-3 unpadding">
            <img src="{{$app['vergo_news.assets']->getPath('/img/yeap.png')}}" class="img-responsive">
        </div>
        <div class="col-md-9">
            <div class="col-xs-6 unpadding">
                <div class="col-xs-3 unpadding small">22 Декабря</div>
                <div class="col-xs-3 unpadding small"><i class="fa fa-cloud green"></i> Ташкент</div>
                <div class="col-xs-6 unpadding small">+4..+6 дождь</div>
            </div>
            <div class="col-xs-6">
                <div class="col-xs-7 small text-right">
                    <a href="#">
                        <i class="fa big">+</i> Отправить материал
                    </a>
                </div>
                <div class="col-xs-3 unpadding small">
                    <a href="#">
                        На русском
                    </a>
                </div>
                <div class="col-xs-2 unpadding small">
                    <a href="#">
                        Подписка
                    </a>
                </div>
            </div>
            <div class="col-xs-12 unpadding menu-list">
                <ul class="list-inline">
                    <li class="text-uppercase">
                        <a href="#">
                            Наука
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Экономика
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Бизнес
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#" class="small">
                            Мода
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Культура
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Видео
                        </a>
                    </li>
                    <li class="text-uppercase">
                        <a href="#">
                            Мода
                        </a>
                    </li>
                    @if(isset(Request::user()->id))
                        <li class="">
                            <span class="btn btn-default btn-rounded">{{Request::user()->getFullName()}}</span>
                        </li>
                        <li>
                            <a href="/site/logout">
                                <span class="btn btn-default btn-rounded">Выйти</span>
                            </a>
                        </li>
                    @else
                        <li class="">
                            <a href="/site/login">
                                <span class="btn btn-default btn-rounded">Войти</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="">
                                <span class="btn btn-default btn-rounded">Регистрация</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</section>