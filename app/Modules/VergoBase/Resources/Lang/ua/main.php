<?php
    return [
        'table_status'      =>  'Статус',
        'table_acts'        =>  'Дії',
        'page_edit'         =>  'Редагування',
        'page_create'       =>  'Створення',
        'btn_create'        =>  'Створити',
        'welcome'           =>  'Вітаю,',
        'menu_main'         =>  'Головна',
        'status_active'     =>  'активна',
        'status_not_chk'    =>  'не перевірена',
        'status_delete'     =>  'видалена',
        'exit'              =>  'Вихід',
        'site'              =>  'на сайт',
        'btn_cancel'        =>  'Відміна',
        'btn_update'        =>  'Оновити'
    ];