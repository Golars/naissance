<?php

return [
    'table_status'      =>  'Status',
    'table_acts'        =>  'Acts',
    'page_edit'         =>  'Edit',
    'page_create'       =>  'Create',
    'btn_create'        =>  'Create',
    'welcome'           =>  'Welcome,',
    'menu_main'         =>  'Main page',
    'status_active'     =>  'active',
    'status_not_chk'    =>  'not checked',
    'status_delete'     =>  'delete',
    'exit'              =>  'Exit',
    'site'              =>  'Back to site',
    'btn_cancel'        =>  'Cancel',
    'btn_update'        =>  'Update'
];