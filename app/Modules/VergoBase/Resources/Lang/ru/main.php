<?php
    return [
        'table_status'      =>  'Статус',
        'table_acts'        =>  'Действия',
        'page_edit'         =>  'Редактирование',
        'page_create'       =>  'Создание',
        'btn_create'        =>  'Создать',
        'welcome'           =>  'Здравствуйте,',
        'menu_main'         =>  'Главная',
        'status_active'     =>  'активная',
        'status_not_chk'    =>  'не провереная',
        'status_delete'     =>  'удалена',
        'exit'              =>  'Выход',
        'site'              =>  'на сайт',
        'btn_cancel'        =>  'Отмена',
        'btn_update'        =>  'Обновить'
    ];