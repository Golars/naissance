<?php
namespace App\Modules\VergoBase\Http\Services;

class Role extends Base{
    protected $modelName = 'App\Modules\VergoBase\Database\Models\Role';
    protected $orderBy = [];

    public function prepareSelect(){
        $roles = ['0'=>'Всем'];
        $model = $this->getModel();
        $this->setWhere(['status'=>$model::STATUS_ACTIVE]);
        foreach($this->getAll() as $role){
            $roles[$role->id] = $role->name;
        }
        return $roles;
    }
}