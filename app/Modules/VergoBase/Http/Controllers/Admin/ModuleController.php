<?php

namespace App\Modules\VergoBase\Http\Controllers\Admin;

class ModuleController extends Controller{
    protected $prefix = 'admin:modules';
    protected $serviceName = 'App\Modules\VergoBase\Http\Services\Module';
}
