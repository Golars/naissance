<?php
namespace App\Modules\VergoBase\Http\Controllers\Admin;

use App;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $prefix = 'admin:users';
    protected $moduleName = 'vergo_base';
    protected $serviceName = 'App\Modules\VergoBase\Http\Services\User';

    private function setAuthPrefix(){
        $this->prefix = 'admin';
    }

    public function login(Request $request){
        $this->setAuthPrefix();
        if ($request->method() == 'GET') {
            return view($this->getViewRoute('login'));
        }
        $this->setRules([
            'login'     =>  'required|min:2|max:60',
            'password'  =>  'required|min:2|max:60'
        ]);
        if ($this->isValidationFails($request)){
            return view($this->getViewRoute('login'), ['errors' => $this->getValidatorErrors()]);
        }
        $this->service->auth($this->getRulesInput($request));

        if($this->service->isErrors()) {
            return view($this->getViewRoute('login'), ['errors' => $this->service->getErrors()]);
        }

        $request->session()->put('token', $this->service->getModel()->token->getApiKey());
        return redirect($this->getRoute());
    }

    public function logout(Request $request){
        $this->setAuthPrefix();
        $request->session()->forget('token');
        return redirect($this->getRoute('login'));
    }

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'roles' => App::make('App\Modules\VergoBase\Http\Services\Role')->prepareSelect(),
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'login'         =>  'required|unique:users|min:2',
            'first_name'    =>  'required|min:3',
            'last_name'     =>  'required|min:5',
            'email'         =>  'required|unique:users|email',
            'cover'         =>  'image',
            'role_id'       =>  'required',
            'password'      =>  'required',
            'file_cover_id' =>  ''
        ]);
        if($request->method() != 'GET' && isset($request['cover'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_cover_id' => $imageId));
        }
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'            =>  'required',
            'login'         =>  'required|min:2',
            'first_name'    =>  'required|min:3',
            'last_name'     =>  'required|min:5',
            'email'         =>  'required|email',
            'cover'         =>  'image',
            'role_id'       =>  'required',
            'password'      =>  'min:3',
            'file_cover_id' =>  ''
        ]);
        if($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_cover_id' => $imageId));
        }
        return parent::edit($request, $id);
    }
    private function saveFile(Request $request){
        $service = new App\Modules\VergoBase\Http\Services\Image($request->file('cover'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }
}