<?php

namespace App\Modules\VergoBase\Http\Controllers\Admin;

use App;
use App\Modules\VergoBase\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController{
    protected $prefix = 'admin';

    protected function index(){
        return view($this->getViewRoute(), ['collection' => $this->service->getAll()]);
    }

    /**
     * @param Request $request
     * @return \App\Modules\VergoBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    protected function add(Request $request){
        if ($request->method() == 'GET') {
            return view($this->getViewRoute('edit'), $this->prepareData());
        }
        return $this->postEdit($request);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\VergoBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    protected function edit(Request $request, $id){
        if (!isset($id)){
            return redirect($this->getRoute());
        }
        $this->service->getOne($id);
        if($this->service->isErrors()) {
            abort(404);
        }
        if ($request->method() == 'GET') {
            return view($this->getViewRoute('edit'), $this->prepareData($this->service->getModel()));
        }
        return $this->postEdit($request,$id);
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function active($id){
        return $this->changeStatus($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function delete($id){
        $model = $this->service->getModel();
        return $this->changeStatus($id, $model::STATUS_DELETED);
    }

    private function changeStatus($id, $status = null){
        if(!$id) {
            abort(404, 'Article is Not Found');
        }

        $this->service->getOne($id);
        $this->service->setStatus($status);
        return redirect($this->getRoute());
    }

    protected function postEdit(Request $request){
        if ($this->isValidationFails($request)){
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        if($this->service->getModel()->fill($this->getRulesInput($request))->save()){
            return $this->sendOk(['goto'=>$this->getRoute()]);
        }
        return $this->sendWithErrors('Something went wrong');
    }

    protected function prepareData($model = null){
        return ['model' => (isset($model->id)) ? $model : $this->service->getModel()];
    }
}
