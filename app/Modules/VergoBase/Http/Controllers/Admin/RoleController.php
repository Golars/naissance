<?php
namespace App\Modules\VergoBase\Http\Controllers\Admin;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $prefix = 'admin:roles';
    protected $moduleName = 'vergo_base';
    protected $serviceName = 'App\Modules\VergoBase\Http\Services\Role';

    public function add(Request $request){
        $this->setRules([
            'name'     =>  'required|unique:roles|min:2',
            'is_admin' =>  '',
        ]);
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'       =>  'required',
            'name'     =>  'required|min:2',
            'is_admin' =>  '',
        ]);
        return parent::edit($request, $id);
    }
}