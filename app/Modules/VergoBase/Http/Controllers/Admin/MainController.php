<?php

namespace App\Modules\VergoBase\Http\Controllers\Admin;

class MainController extends Controller{

    public function index(){
        return view($this->getViewRoute());
    }
}
