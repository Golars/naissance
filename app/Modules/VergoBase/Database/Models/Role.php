<?php
namespace App\Modules\VergoBase\Database\Models;

/**
 * @property $is_admin
 * @property $name
 * @property $status
 */
class Role extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('name', 'is_admin', 'status');

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Modules\VergoBase\Database\Models\User');
    }

    public function setDefault(){
        $this->id         = 0;
        $this->is_admin   = 0;
        $this->name       = 'Удален';
        return $this;
    }

    public function setIsAdminAttribute($value){
        $this->attributes['is_admin'] = (isset($value));
    }
}
